'use strict';

/* Controllers */
var flickrControllers = angular.module('flickrControllers', []);
var apiKey = 'here must be your flickr api key';

flickrControllers.controller('GalleryCtrl', ['$scope', '$http', '$cookies',
    function ($scope, $http, $cookies) {
        $scope.isSearching = false;
        $scope.orderProp = 'dateupload';

        $scope.search = function () {
            $scope.isSearching = true;
            $http({
                method: 'GET',
                url: 'https://api.flickr.com/services/rest',
                params: {
                    method: 'flickr.photos.search',
                    api_key: apiKey,
                    text: $scope.searchTerm,
                    format: 'json',
                    nojsoncallback: 1,
                    extras: 'date_upload'
                }
            }).success(function (data) {
                $scope.photos = data.photos.photo;
                $scope.photos.forEach(function (photo) {
                    photo.imageUrl = 'https://farm' + photo.farm + '.staticflickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_b.jpg';
                    photo.dateupload *= 1000;
                    photo.dateupload_down = -photo.dateupload;
                });
                $scope.isSearching = false;
            }).error(function (error) {
                console.error(error);
                $scope.isSearching = false;
            })
        }
    }]);


flickrControllers.controller('PhotoDetailCtrl', ['$scope', '$routeParams', '$http',
    function ($scope, $routeParams, $http) {
        $http({
            method: 'GET',
            url: 'https://api.flickr.com/services/rest',
            params: {
                method: 'flickr.photos.getInfo',
                api_key: apiKey,
                photo_id: $routeParams.photoId,
                format: 'json',
                nojsoncallback: 1
            }
        }).success(function (data) {
            $scope.photo = data.photo;
            $scope.photo.imageUrl = 'https://farm' + $scope.photo.farm + '.staticflickr.com/' + $scope.photo.server + '/' + $scope.photo.id + '_' + $scope.photo.secret + '_b.jpg';
            $scope.photo.dateupload *= 1000;
            $scope.photo.dates.lastupdate *= 1000;
            $scope.photo.dates.posted *= 1000;
            $scope.photo.dates.taken = (new Date($scope.photo.dates.taken)).getTime();
            $scope.photo.description = $scope.photo.description._content;
            $scope.photo.title = $scope.photo.title._content;

            var tags = [];
            if ($scope.photo.tags.tag[0]) {
                $scope.photo.tags.tag.forEach(function (tag) {
                    tags.push(tag._content);
                });

            }
            $scope.photo.tags = tags.join(', ');

        }).error(function (error) {
            console.error(error);
            $scope.isSearching = false;
        })
    }]);