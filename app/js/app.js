'use strict';

/* App Module */
var flickrApp = angular.module('flickrApp', ['ngMaterial', 'ngCookies', 'ngRoute', 'flickrControllers']);

flickrApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/photos', {
                templateUrl: 'partials/photo-list.html',
                controller: 'GalleryCtrl'
            }).
            when('/photos/:photoId', {
                templateUrl: 'partials/photo-detail.html',
                controller: 'PhotoDetailCtrl'
            }).
            otherwise({
                redirectTo: '/photos'
            });
    }]);
